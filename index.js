/**
 * Express
 */
const express = require('express');
const app = express();

var server = require('http').createServer(app);


// app.set('socketio', io);
// app.io = io;

// var io = req.app.get('socketio'); 

var io = require('socket.io')(server);
io.on('connection', function (socket) {
    socket.on("messages", (data) => {
        socket.emit("messages", data);
        socket.broadcast.emit("messages", data);
        console.log(data);
    });
});



/**
 * Mongoose 
 */
var mongoose = require('mongoose');
var mongoDB = 'mongodb://127.0.0.1/shipment_db';
mongoose.connect(mongoDB, { useNewUrlParser: true });
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));


/**
 * Cors
 */
var cors = require('cors');
app.use(cors());

 

/**
 * Router
 */
var ShipmentRouter = require(process.cwd() + '/router/shipment');
app.use('/api/shipment', ShipmentRouter);

var UserRouter = require(process.cwd() + '/router/user')
app.use('/user', UserRouter);

var ChatRouter = require(process.cwd() + '/router/chat')
app.use('/api/chat', ChatRouter);

var personalRouter = require(process.cwd() + '/router/personal')
app.use('/api/personal', personalRouter);

// var SocketRouter = require(process.cwd()+'/router/socket') 
// app.use('/socket', SocketRouter); 



/**
 * Cread server
 */
server.listen(8081, () => {
    var host = server.address().address;
    var port = server.address().port;
    console.log("Example app listening at http://%s:%s", host, port);
});

// var PORT = process.env.PORT || 8081;
// //var PORT = 3000;
// server.listen(PORT, ()=> {
//   console.log('Node/Express: \x1b[32m%s\x1b[0m', 'online - port: '+ PORT);
// });

