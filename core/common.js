/**
 * Controller Config
 */
const Config = require(process.cwd()+'/core/config');
/**
 * Controller Constant
 */
const Constant = require(process.cwd()+'/core/constant');

/**
 * Base Url + uri
 */ 
exports.baseUrl = (uri = '') => {
    return Config.baseUrl+uri
}

/**
 * Base Url API + uri
 */ 
exports.baseUrlApi  = (uri = '') => {
    return Config.baseUrlApi+uri
}

/**
 * Function Request for Fontend
 */ 
exports.responses = (res,data,status = 200)  => {
    res.header("Access-Control-Allow-Origin", "*");
    res.status(status);
    res.json(data);
}


/** 
 * CheckTimeToken
 */ 
exports.checkTimeToken = (token)  => {
    var timeToken = token.split(".")[1];    
    var timeCurrent = new Date().getTime(); 
    var timeLogin = timeCurrent-timeToken
    if(timeLogin>Constant.timeLoginLimit) return false
    return true;
}


/**
 * Function Common
 * Make ID shipment
 */
 
exports.makeRef = (length = 10)  => {
    Number(length);
    if(length<1) {
        length = 10;
    }
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

/**
 * Function Common
 * Make ID 
 */
 
exports.makeId = (length = 10)  => {
    Number(length);
    if(length<1) {
        length = 10;
    }
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


/**
 * Function Common
 * Get weight
 */
exports.getWeight = (weight = 0)  => {
    Number(weight);
    weight = weight * 1000;
    if (weight < 1) {weight = "weight";};
    if (weight < 250) {weight = 250};
    if (weight > 15000) {weight = 15001;} ;
    return weight;
}


/**
 * Function Common
 * responses Data Ship Return
 */
exports.responsesDataShip = (data = {})  => {
    return {
        "data": data ? data : [],
    };
};

/**
 * Function Common
 * responses Data Return
 */
exports.responsesData = (data = {}, mesenger = "Successful", code) => {
    return {
        "status": code?code:200,
        "success": code > 200 ? false: true,
        "message": `${mesenger}`,     
        "data": data ? data : [],
    };
};
