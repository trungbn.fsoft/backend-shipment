/**
 * Table shipment
 */ 
exports.personal =  {
    name: {type: String}, 
    image :  {type: String},    
    address :  {type: String}, 
    created_at : { type : Date, default: Date.now },
    ref :  { type: String},
 };

