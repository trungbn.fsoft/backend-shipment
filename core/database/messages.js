/**
 * Table messages
 */ 
exports.messages =  { 
    messages: {
      type: String, 
      required: true
    }, 
    created_at : { type : Date, default: Date.now },
    update_at : { type : Date, default: Date.now }, 
  };

