/**
 * Table shipment
 */ 
exports.shipments =  {
    quote: {
        id: {type: String}, 
    },
    origin: {
        contact: {
            name: {type: String},
            email: {type: String},
            phone: {type: String},
        },
        address: {
            country_code: {type: String},
            locality: {type: String},
            postal_code: {type: Number, default: 0},
            address_line1: {type: String},
        }
    },
    destination: {
        contact: {
            name: {type: String},
            email: {type: String},
            phone: {type: String},
        },
        address: {
            country_code: {type: String},
            locality: {type: String},
            postal_code: {type: Number, default: 0},
            address_line1: {type: String}
        }
    },
    package: {
        dimensions: {
            height: {type: Number, default: 0},
            width: {type: Number, default: 0},
            length: {type: Number, default: 0},
            unit: {type: String}
        },
        grossWeight: {
            amount: {type: Number, default: 0},
            unit: {type: String},
        }
    },
    created_at : { type : Date, default: Date.now },
    ref :  { type: String},
    cost :  { type: String},
};

