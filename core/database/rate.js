/**
* Table rate
*/
exports.rate = {
    weight: { type: Number, unique: true, required: true },
    price: { type: Number, required: true },
    from_country: { type: String, required: true },
    to_country: { type: String, required: true },
};
