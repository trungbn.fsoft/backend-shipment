var MongoClient = require('mongodb').MongoClient;
url = "mongodb://127.0.0.1/";
var nameDb = "shipment_db"

MongoClient.connect(url, function (err, db) {
    if (err) throw err;
    var dbo = db.db(nameDb);

    exports.getData = (collection, query, callback) => {
        dbo.collection(collection).find(query).toArray(callback);
    }

    exports.createData = (collection, object, callback) => {
        dbo.collection(collection).insertOne(object, callback);
    }

    exports.updateMany = (collection, filter, update, callback) => {
        dbo.collection(collection).findAndModify(
            filter,
            [['_id', 'asc']],  // sort order
            { $set: update },
            { new: true },
            callback
        );
    }
});
