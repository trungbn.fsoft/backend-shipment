/**
 * Config
 */
const Config = require(process.cwd()+'/core/config');

/**
 * Constant
 */
const Constant = require(process.cwd()+'/core/constant');

/**
 * Model 
 */
const Model = require(process.cwd()+'/core/model');

/**
 * Common
 */
const Common = require(process.cwd()+'/core/common');


/**
 * Main
 */ 
exports.main = () => {
    return "main"
}

/**
 * Main.config
 */ 
exports.config =   Config;
/**
 * Main.constant
 */ 
exports.constant =   Constant;

/**
 * Main.common
 */ 
exports.common =  Common ;


/**
 * Main.model
 */ 
exports.model =  Model ;