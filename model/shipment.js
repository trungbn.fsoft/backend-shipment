//https://mongoosejs.com/docs/api.html#
//Require Mongoose
let mongoose = require('mongoose');

//Định nghĩa một schema
let Schema = mongoose.Schema;


/**
 * Controller
 */
const Common = require(process.cwd() + '/core/common');

/**
 * Name table
 */
let nameCollection = "shipments";
let id = "ref";

/**
 * Database
 */
const Database = require(process.cwd() + '/core/database/'+nameCollection);

let collection = JSON.parse(JSON.stringify(Database[nameCollection])); 

let collectionSchema = new Schema(collection);

collectionSchema.statics.getList = function getList(query, choose = {}, callback) {
    var chooseQuery = {
        select: choose.select ? choose.select : [],
        skip: choose.skip ? choose.skip : 0,
        limit: choose.limit ? choose.limit : 0,
        sort: choose.sort ? choose.sort : { created_at: -1 },
    };
    return this.find(query, chooseQuery.select, { skip: chooseQuery.skip, limit: chooseQuery.limit, sort: chooseQuery.sort }).exec(callback);
};

collectionSchema.statics.saveRow = function saveRow(bodyCollection, callback) {
    if (bodyCollection[id]) {
        return this.findOneAndUpdate({ ref: bodyCollection[id] }, bodyCollection, { new: true }, callback);
    } else {
        bodyCollection[id] = Common.makeRef(10);
        return this.create(bodyCollection, callback);
    }
};

// function = deleteMany(query,callback)

// function = findOneAndRemove(query,callback)

// function = findByIdAndRemove(query,callback)

//function = count(query,callback) 



module.exports = mongoose.model(nameCollection, collectionSchema, nameCollection);
