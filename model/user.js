//Require Mongoose
var mongoose = require('mongoose');

//Định nghĩa một schema
var Schema = mongoose.Schema;

var userSchema = new Schema({
  id: {
    type: String,
    unique: true,
    required: true
  },
  email: {
    type: String,
    unique: true,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    type: String,
    required: true
  },  
  token: {
    type: String, 
  },  
  role: {
    type: Number,
    required: true
  },
  created_at : { type : Date, default: Date.now },
  update_at : { type : Date, default: Date.now },
  last_login : { 
    type : Date, 
    default: Date.now
   },
});
module.exports = mongoose.model('Users', userSchema, 'user');
