var express = require('express');
var app = express();
var server = require('http').createServer(app);

var io = require('socket.io')(server);
io.on('connection', function(socket) {
   
    socket.on("messages", (data) => {
        socket.emit("messages", data);
        socket.broadcast.emit("messages", data);
        console.log(data);
    });
});

var PORT = process.env.PORT || 8080;
//var PORT = 3000;
server.listen(PORT, ()=> {
  console.log('Node/Express: \x1b[32m%s\x1b[0m', 'online - port: '+ PORT);
});

console.log("socket start");
