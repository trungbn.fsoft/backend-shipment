const Common = require(process.cwd() + '/controller/common');

test('_getWeight : input Weight < - 1 , output : weight (error)', () => {
    expect(Common._getWeight(-1)).toBe('weight');
});

test('_getWeight : input Weight  < 0.250 KG , out put 250 g', () => {
    expect(Common._getWeight(0.020)).toBe(250);
});

test('_getWeight : input Weight  > 15.000 KG, output 15001 g', () => {
    expect(Common._getWeight(15.100)).toBe(15001);
});

test('_getWeight: : input Weight  == Weight', () => {
    expect(Common._getWeight(9)).toBe(9000);
});


test('_makeRef: input = 10 > str.length == 10', () => {
    expect(Common._makeRef(10).length).toBe(10);
});

test('_makeRef: input < 1 str.length == 10 default value', () => {
    expect(Common._makeRef(-1).length).toBe(10);
});



