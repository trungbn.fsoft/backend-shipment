var express = require('express')
var router = express.Router()

//controler
 const UserControler = require(process.cwd()+'/controller/user');
 const Main = require(process.cwd()+'/core/main');

// body Parser
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();



/**
 * Resgister
 * @constructor
 * @method POST
 */
router.post('/login',jsonParser, (req, res) => { 
    UserControler.login(req).then((data) => {
        Main.common.responses(res,data,data.status); 
    }).catch((err) => {
        Main.common.responses(res,err,500)
    });
});




/**
 * Resgister
 * @constructor
 * @method POST
 */
router.post('/register',jsonParser, (req, res) => { 
    UserControler.register(req).then((data) => {
        Main.common.responses(res,data,data.status); 
    }).catch((err) => {
        Main.common.responses(res,err,500)
    });
});

/**
 * List user
 * @constructor
 * @method GET
 */
router.get('/getlist',jsonParser, (req, res) => {  
    UserControler.getList(req).then((data) => {
        Main.common.responses(res,data,data.status); 
    }).catch((err) => {
        Main.common.responses(res,err,500)
    }); 
});

/**
 * Get user
 * @constructor
 * @method GET
 */
router.post('/getUser',jsonParser, (req, res) => {  
    UserControler.getUser(req).then((data) => {
        Main.common.responses(res,data,data.status); 
    }).catch((err) => {
        Main.common.responses(res,err,500)
    }); 
});




router.use(function(req, res, next) { 
    // input
    var arr1 = {
        key1 :"key1",
        key2 : "key2",
        key1_1 : "key1_1",
    }
    
    var arr2 = {
        key1 : "key1_new",
        key3 : "key3",
        key2 : "key2_new",
    }

    // let newArr1 = { ...arr1 }; 
    // console.log(newArr1);
    // console.log(arr1);
    
// Object.keys(arr1).forEach(key => { 
//     // if(arr2[key])
//         arr1[key] = arr2[key]; 
// });
// console.log(arr1);



    //output
    arr1 = {
        key1 :"key1_new",
        key2 : "key2_new",
    } 


    
    Main.common.responses(res,Main.common.responsesData({error:"API not found or method not support"},"Error",500),500)
});









module.exports = router


