var express = require('express')
var router = express.Router()


//controler
const PersonalControler = require(process.cwd() + '/controller/personal');
const UserControler = require(process.cwd() + '/controller/user');
const Main = require(process.cwd() + '/core/main');

// body Parser
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json({limit: '50mb'});








/**
 * Test callback Model
 * @method GET
 */
router.get('/getPersonalListCallmodel', (req, res) => {
    PersonalControler.getPersonalListCallModel(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    });
});

/**
 * API Get List personal
 *  * @method GET
 */
router.get('/getPersonalList', (req, res) => {
    PersonalControler.getPersonalList(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});
/**
 * API Get List personal Total
 *  @method GET
 */
router.get('/getPersonalListTotal', (req, res) => {
    PersonalControler.getPersonalListTotal(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

// res.header("Access-Control-Allow-Origin", "*");
// res.json(_ResponseData(data, mesenger, ""));
/**
 * API Get Detail personal
 * @method GET
 * @param {ref} id personal.
 */
router.get('/getPersonal/:ref', (req, res) => {
    PersonalControler.getPersonal(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

/**
 *API Get Detail personal
 * @method GET
 * @param {ref} id personal.
 */
router.delete('/delete/:ref', (req, res) => {
    PersonalControler.deletePersonal(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});



/**
 *API Add personal
 * @method POST
 * @param {object} info personal.
 */
router.post('/createPersonal', jsonParser, (req, res) => { 
    PersonalControler.createPersonal(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

/**
 * rateAdd end addManny
 * @constructor Array[]
 * @method POST
 * @param {weight} weight rate.
 * @param {price} price rate.
 * @param {from_country} from country rate.
 * @param {to_country} to country rate.
 */
router.post('/rate/add', jsonParser, (req, res) => { PersonalControler.rateAdd(req, res) });

/**
 * rateAdd end addManny
 * @constructor
 * @method POST
 * @param {weight} weight rate.
 * @param {price} price rate.
 * @param {from_country} from country rate.
 * @param {to_country} to country rate.
 */
router.post('/rate/add-many', jsonParser, (req, res) => { PersonalControler.rateAddMany(req, res) });



module.exports = router


