var express = require('express')
var router = express.Router()


//controler
const ChatControler = require(process.cwd() + '/controller/chat'); 
const Main = require(process.cwd() + '/core/main');

// body Parser
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();



router.post('/add-messenge', jsonParser ,(req, res) => { 
    ChatControler.addMessages(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, err, 500)
    });
});

router.post('/list-messenge', jsonParser ,(req, res) => { 
  
    ChatControler.getMessages(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, err, 500)
    });
});

router.post('/new-messenge', jsonParser ,(req, res) => { 
  
    ChatControler.getNewMesenger(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, err, 500)
    });
});


 
 
// router.post('/api/chat/add-messenge', jsonParser, (req, res) => { ChatControler.addMessages(req) });



module.exports = router


