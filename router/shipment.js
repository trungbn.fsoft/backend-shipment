var express = require('express')
var router = express.Router()


//controler
const ShipmentControler = require(process.cwd() + '/controller/shipment');
const UserControler = require(process.cwd() + '/controller/user');
const Main = require(process.cwd() + '/core/main');

// body Parser
const bodyParser = require('body-parser');
const jsonParser = bodyParser.json();

// router.use(function(req, res, next) { 
//   if((req.originalUrl=='/user/login')) {
//     next();
//   } else {
//     if(req.headers.authorization && Main.common.checkTimeToken(req.headers.authorization)) {
//         UserControler.checkToken(req).then((data) => {  
//               if(data.status==200) {
//                 next();
//               }  else {
//                 res.json({ status: 409,
//                   success: false,
//                   message: 'Token error',
//                   data: []
//                 });
//               }         
//           }).catch((err) => {
//               res.json({ status: 409,
//                 success: false,
//                 message: 'Token error',
//                 data: []
//               });
//           });  
//       } else {
//         res.json({ status: 409,
//           success: false,
//           message: 'Token error',
//           data: []
//          });
//       }
//   } 
// });






/**
 * Test callback Model
 * @method GET
 */
router.get('/getShipmentListCallmodel', (req, res) => {
    ShipmentControler.getShipmentListCallModel(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    });
});

/**
 * API Get List shipment
 *  * @method GET
 */
router.get('/getShipmentList', (req, res) => {
    ShipmentControler.getShipmentList(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});
/**
 * API Get List shipment Total
 *  @method GET
 */
router.get('/getShipmentListTotal', (req, res) => {
    ShipmentControler.getShipmentListTotal(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

// res.header("Access-Control-Allow-Origin", "*");
// res.json(_ResponseData(data, mesenger, ""));
/**
 * API Get Detail shipment
 * @method GET
 * @param {ref} id Shipment.
 */
router.get('/getShipment/:ref', (req, res) => {
    ShipmentControler.getShipment(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

/**
 *API Get Detail shipment
 * @method GET
 * @param {ref} id Shipment.
 */
router.delete('/delete/:ref', (req, res) => {
    ShipmentControler.shipmentDelete(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

/**
 * API Get Amount shipment
 * @method POST
 * @param {object}  info weight Shipment.
 */
router.post('/getquote', jsonParser, (req, res) => {
    ShipmentControler.getQuote(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, err, 500)
    })
}
);

/**
 *API Add shipment
 * @method POST
 * @param {object} info Shipment.
 */
router.post('/createshipment', jsonParser, (req, res) => {
    ShipmentControler.createShipment(req).then((data) => {
        Main.common.responses(res, data, data.status)
    }).catch((err) => {
        Main.common.responses(res, data, 500)
    })
});

/**
 * rateAdd end addManny
 * @constructor Array[]
 * @method POST
 * @param {weight} weight rate.
 * @param {price} price rate.
 * @param {from_country} from country rate.
 * @param {to_country} to country rate.
 */
router.post('/rate/add', jsonParser, (req, res) => { ShipmentControler.rateAdd(req, res) });

/**
 * rateAdd end addManny
 * @constructor
 * @method POST
 * @param {weight} weight rate.
 * @param {price} price rate.
 * @param {from_country} from country rate.
 * @param {to_country} to country rate.
 */
router.post('/rate/add-many', jsonParser, (req, res) => { ShipmentControler.rateAddMany(req, res) });



module.exports = router


