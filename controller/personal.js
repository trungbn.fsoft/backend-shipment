/**
 * Npm
 */
const md5 = require("blueimp-md5");
const fs = require("fs");

/**
 * Model
 */
const PersonalModel = require(process.cwd() + '/model/personal');

/**
 * Main Controller
 */
const Main = require(process.cwd() + '/core/main');


// const Model = require(process.cwd()+'/core/model');



/**
 * List Personal
 */
exports.getPersonalList = (req) => {

    return new Promise((resolve, reject) => {
        let numberPerPage = 10;
        let skip = (req.query.page - 1) * numberPerPage;
        PersonalModel.getList({}, { skip: skip }, (err, data) => {
            let mesenger = "List Successful"
            if (err) {
                mesenger = "Error";
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData(data, mesenger, 200));
        });
    });
};

/**
 * List Personals Total
 */
exports.getPersonalListTotal = (req) => {
    return new Promise((resolve, reject) => {
        PersonalModel.count({}, (err, dataCount) => {
            let mesenger = "List Successful";
            if (err) {
                mesenger = "Error";
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData({ count: dataCount }, mesenger, 200));
        });
    });
};

/**
 * Detail Personal
 *  @input {ref} id Personal.
 */
exports.getPersonal = (req) => {
    return new Promise((resolve, reject) => {
        const ref = req.params.ref;
        PersonalModel.findOne({ ref: ref }, (err, data) => {
            let mesenger = "Detail Successful"
            if (err) reject(Main.common.responsesData(err, "Error", ""));
            resolve(Main.common.responsesData(data, mesenger, ""));
        });
    });
};

/**
 * Delete Personal 
 * @input {ref} id Personal.
 */
exports.deletePersonal = (req) => {
    return new Promise((resolve, reject) => {
        const ref = req.params.ref;
        PersonalModel.findOneAndRemove({ ref: ref }, (err, data) => {
            let mesenger = "Your Personal has been deleted."
            if (err) reject(Main.common.responsesData(err, "Error", 500));
            if (data == null) {
                mesenger = "Personal has not found."
                resolve(Main.common.responsesData(data, mesenger, 404));
            } else {
                resolve(Main.common.responsesData(data, mesenger, ""));
            }
        }
        );
    });
};

 

/**
 * Add Personal
 * @method post 
 * @input {object} info Personal.
 */
exports.createPersonal = (req) => {
    return new Promise((resolve, reject) => {
        const personalBody = req.body.data;
        PersonalModel.saveRow(personalBody, (err, dataAdd) => {
            responseAdd = {
                "ref" : dataAdd['ref'],
                "created_at" : dataAdd['created_at'],
            };
            let mesenger = "Sucess"
            if (err) reject(Main.common.responsesDataShip(err, "Error", 500));
            resolve(Main.common.responsesDataShip(responseAdd, mesenger, ""));
        });
    });
};

/**
 * Add Rate Many 
 */
exports.rateAddMany = (req, res) => {
    const dirname = __dirname + './..';
    var rates = fs.readFileSync(dirname + "/rates.json", "utf8");
    rates = JSON.parse(rates);
    RateModel.insertMany(rates, function (err, data) {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(Main.common.responsesDataShip(data));
    });
};

/**
 * Add Rate one
 * @input {object} info Rate.
 */
exports.rateAdd = (req, res) => {
    var rate = req.body;
    RateModel.create(rate, (err, data) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(Main.common._responsesDataShip(data));
    });
};



