/**
 * Npm
 */
const md5 = require("blueimp-md5");
const fs = require("fs");

/**
 * Model
 */
const ShipmentModel = require(process.cwd() + '/model/shipment');
const RateModel = require(process.cwd() + '/model/rate');

/**
 * Main Controller
 */
const Main = require(process.cwd() + '/core/main');


// const Model = require(process.cwd()+'/core/model');



/**
 * List Shipments
 */
exports.getShipmentList = (req) => {
    // Main.model.getData('shipments',{},(err, data)=>{
    //     console.log(data);
    // });

    return new Promise((resolve, reject) => {
        let numberPerPage = 10;
        let skip = (req.query.page - 1) * numberPerPage;
        ShipmentModel.getList({}, { skip: skip }, (err, data) => {
            let mesenger = "List Successful"
            if (err) {
                mesenger = "Error";
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData(data, mesenger, 200));
        });
    });
};

/**
 * List Shipments Total
 */
exports.getShipmentListTotal = (req) => {
    return new Promise((resolve, reject) => {
        ShipmentModel.count({}, (err, dataCount) => {
            let mesenger = "List Successful";
            if (err) {
                mesenger = "Error";
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData({ count: dataCount }, mesenger, 200));
        });
    });
};

/**
 * Detail Shipments
 *  @input {ref} id Shipment.
 */
exports.getShipment = (req) => {
    return new Promise((resolve, reject) => {
        const ref = req.params.ref;
        ShipmentModel.findOne({ ref: ref }, (err, data) => {
            let mesenger = "Detail Successful"
            if (err) reject(Main.common.responsesData(err, "Error", ""));
            resolve(Main.common.responsesData(data, mesenger, ""));
        });
    });
};

/**
 * Delete Shipments 
 * @input {ref} id Shipment.
 */
exports.shipmentDelete = (req) => {
    return new Promise((resolve, reject) => {
        const ref = req.params.ref;
        ShipmentModel.findOneAndRemove({ ref: ref }, (err, data) => {
            let mesenger = "Your Shipment has been deleted."
            if (err) reject(Main.common.responsesData(err, "Error", 500));
            if (data == null) {
                mesenger = "Shipment has not found."
                resolve(Main.common.responsesData(data, mesenger, 404));
            } else {
                resolve(Main.common.responsesData(data, mesenger, ""));
            }
        }
        );
    });
};

/**
 * GetAmount
 * @method post 
 * @input {object} info Shipment.
 */
exports.getQuote = (req) => {
    return new Promise((resolve, reject) => {
        const shipmentBody = req.body.data;
        const weight = Main.common.getWeight(shipmentBody.package.grossWeight.amount);
        RateModel.findOne({ weight: { $lte: weight } }, [], {
            skip: 0,
            limit: 1,
            sort: { weight: -1 }
        }, (err, data) => { var dataAmount;
            dataAmount = [{
                "id": data['_id'],
                "amount": data['price'],
            }];
            let mesenger = "Sucess";
            if (err) reject(Main.common.responsesDataShip(err, "Error", 500));
            resolve(Main.common.responsesDataShip(dataAmount, mesenger, ""));
        }
        );

    });
};

/**
 * Add shipment
 * @method post 
 * @input {object} info Shipment.
 */
exports.createShipment = (req) => {
    return new Promise((resolve, reject) => {
        const shipmentBody = req.body.data;
        ShipmentModel.saveRow(shipmentBody, (err, dataAdd) => {
            responseAdd = {
                "ref" : dataAdd['ref'],
                "created_at" : dataAdd['created_at'],
                "cost" : dataAdd['cost'],  // USD
            };
            let mesenger = "Sucess"
            if (err) reject(Main.common.responsesDataShip(err, "Error", 500));
            resolve(Main.common.responsesDataShip(responseAdd, mesenger, ""));
        });
    });
};

/**
 * Add Rate Many 
 */
exports.rateAddMany = (req, res) => {
    const dirname = __dirname + './..';
    var rates = fs.readFileSync(dirname + "/rates.json", "utf8");
    rates = JSON.parse(rates);
    RateModel.insertMany(rates, function (err, data) {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(Main.common.responsesDataShip(data));
    });
};

/**
 * Add Rate one
 * @input {object} info Rate.
 */
exports.rateAdd = (req, res) => {
    var rate = req.body;
    RateModel.create(rate, (err, data) => {
        res.header("Access-Control-Allow-Origin", "*");
        res.json(Main.common._responsesDataShip(data));
    });
};



