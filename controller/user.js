/**
 * Npm
 */
const md5 = require("blueimp-md5");
const fs = require("fs");

/**
 * Model
 */
const UserModel = require(process.cwd()+'/model/user');

 
/**
 * Main Controller
 */
const Main = require(process.cwd()+'/core/main');


/**
 * Login
 * @input email of user.
 * @input password of user.
 */
exports.login = (req) => { 
    return new Promise((resolve, reject) => {
        const userBody = req.body;
        const email = userBody['email'];
        const password = md5(userBody['password']);
        var timeCurrentInt = new Date().getTime(); 
        UserModel.findOneAndUpdate({$and:[{email:email},{password:password}]}, 
            {$set:{token:"token."+timeCurrentInt}}, {new: true}, (err, data) =>   {
            var mesenger = "Login user sucess";
            var code = 200;
            if(data==null) {
                mesenger = "Email pasword not math";
                code = 202;
            }  
            if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
            resolve(Main.common.responsesData(data, mesenger, code));
        });
    });
}; 

/**
 * Check token
 * @input req.headers.authorization
 */
exports.checkToken = (req) => {   
    return new Promise((resolve, reject) => {
        const token =  req.headers.authorization;  
        UserModel.findOne({token:token}, (err, data) => {
            var mesenger = "Token access";
            var code = 200;
            if(data==null) {
                mesenger = "expired token";
                code = 202;
            }  
            if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
            resolve(Main.common.responsesData(data, mesenger, code)); 
        });
    });
};


/**
 * Add User
 * @input {object} info User.
 */
exports.register = (req) => {
    return new Promise((resolve, reject) => {
        const userBody = req.body;
        userBody['id'] = Main.common.makeId(Main.constant.lengthId);
        userBody['password'] = md5(userBody['password']);
        UserModel.create(userBody, (err, data) => {
            let mesenger = "Register user sucess"
            if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
            resolve(Main.common.responsesData(data, mesenger, ""));
        });
    });
};


/**
 * List User
 */
exports.getList = (req) => {
    return new Promise((resolve, reject) => { 
        console.log("Fdsfds");
        UserModel.find({} ,(err,data)=>{
            let mesenger = "List Successful"
            if (err) { 
                mesenger = "Error"; 
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData(data, mesenger, 200));
        })
    });
};

/**
 * List User
 */
exports.getUser = (req) => {
    const query = req.body;
    return new Promise((resolve, reject) => { 
        UserModel.find(query ,(err,data)=>{
            let mesenger = "List Successful"
            if (err) { 
                mesenger = "Error"; 
                reject(Main.common.responsesData(err, mesenger, 500));
            }
            resolve(Main.common.responsesData(data, mesenger, 200));
        })
    });
};


/**
 * Delete User
 * @input {id} id User.
 */
exports.delete = (req) => {
    return new Promise((resolve, reject) => {
        const id = req.params.id;
        UserModel.findOneAndRemove({ id: id }, (err, data) => {
            let mesenger = "Your User has been deleted."
            if (err) reject(Main.common.responsesData(err, "Error", 500));
            if(data==null) {
                mesenger = "User has not found."
                resolve(Main.common.responsesData(data, mesenger, 404));
            } else {
                resolve(Main.common.responsesData(data, mesenger, ""));
            }
        }
        );
    });
};




