/**
 * Npm
 */
 
const app = require('express')();
const server = require('http').createServer(app);
const io = require('socket.io')(server);
// /**
//  * Cors
//  */
// var cors = require('cors');
// app.use(cors());

const documents = {};


/**
 * Main Controller
 */
const Main = require(process.cwd() + '/core/main');

const Model = require(process.cwd() + '/core/model');



exports.chatMessages = (req) => { 
    var io = req.app.get('socketio'); 
    io.on('connection', (socket) => { 
        socket.on("messages", (data) => {
            socket.emit("messages", data);
            socket.broadcast.emit("messages", data);
            console.log("data");
        });
    }); 
};


exports.addMessages = (req) => {
    return new Promise((resolve, reject) => {
        const messages = req.body;
        messages['time'] = new Date().getTime();
        messages['see'] = 0;
        Model.createData("messages", messages, (err, data) => {
            var mesenger = "Create messages sucess";
            var code = 200;
            if (data == null) {
                mesenger = "Create messages Failed";
                code = 202;
            }
            if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
            resolve(Main.common.responsesData(data.ops, mesenger, code));
        });
    });
};

exports.getNewMesenger = (req) => {
    return new Promise((resolve, reject) => {
        const user = req.body;
        Model.updateMany("messages",
            {
                $or: [
                    {
                        "user_send": user['user_send'],
                        "user_receive": user['user_receive'],
                        "see": 0

                    },
                    {
                        "user_send": user['user_receive'],
                        "user_receive": user['user_send'],
                        "see": 0
                    }
                ]
            },
            {
                "see": 1,
            },
            (err, data) => {
                var mesenger = "List success";
                var code = 200;
                var dataNewMsg = [data.value];
                if (data.value == null) {
                    dataNewMsg = [];
                    mesenger = "Error";
                    code = 202;
                } 
                if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
                resolve(Main.common.responsesData(dataNewMsg, mesenger, code));
            });
    });
};

exports.getMessages = (req) => {
    return new Promise((resolve, reject) => {
        const user = req.body;
        Model.getData("messages",
            {
                $or: [
                    {
                        "user_send": user['user_send'],
                        "user_receive": user['user_receive'],
                        // "see": 1

                    },
                    {
                        "user_send": user['user_receive'],
                        "user_receive": user['user_send'],
                        // "see": 1
                    }
                ]
            },
            (err, data) => {
                var mesenger = "List success";
                var code = 200;
                if (data == null) {
                    mesenger = "Error";
                    code = 202;
                }
                if (err) reject(Main.common.responsesData(err, err.errmsg, 500));
                resolve(Main.common.responsesData(data, mesenger, code));
            });
    });
};

